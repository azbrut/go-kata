module gitlab.com/azbrut/go-kata

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/json-iterator/go v1.1.12
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421
	github.com/modern-go/reflect2 v1.0.2
	github.com/ptflp/godecoder v0.0.1
	gitlab.com/azbrut/greet v0.0.0-20230402122846-39fd7c394942
)

require (
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
