package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/roadmap",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 20900,
		},
		{
			Name:  "https://github.com/docker/build-push-action",
			Stars: 3000,
		},
		{
			Name:  "https://github.com/docker/compose-cli",
			Stars: 901,
		},
		{
			Name:  "https://github.com/docker/extensions-sdk",
			Stars: 99,
		},
		{
			Name:  "https://github.com/docker/index-cli-plugin",
			Stars: 37,
		},
		{
			Name:  "https://github.com/docker/docs",
			Stars: 3735,
		},
		{
			Name:  "https://github.com/docker/buildx",
			Stars: 2374,
		},
		{
			Name:  "https://github.com/docker/cli",
			Stars: 3835,
		},
		{
			Name:  "https://github.com/docker/docker-py",
			Stars: 6080,
		},
		{
			Name:  "https://github.com/docker/docker-ce-packaging",
			Stars: 157,
		},
		{
			Name:  "https://github.com/docker/extensions-submissions",
			Stars: 6,
		},
	}

	// в цикле запишите в map
	m := make(map[string]int)
	for _, project := range projects {
		m[project.Name] = project.Stars
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, value := range m {
		fmt.Println(value)
	}
}
