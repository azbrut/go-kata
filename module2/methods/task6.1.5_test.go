package main

import "testing"

func TestCalc(t *testing.T) {
	calc := NewCalc()

	type args struct {
		a        float64
		b        float64
		function func(a, b float64) float64
	}

	tests := []struct {
		name string
		args args
		want float64
	}{
		//multiply
		{
			name: "multiply #1 ",
			args: args{a: 12, b: 12, function: multiply},
			want: 12 * 12,
		},
		{
			name: "multiply #2",
			args: args{a: 0, b: 0, function: multiply},
			want: 0 * 0,
		},
		{
			name: "multiply #3",
			args: args{a: 125, b: 125, function: multiply},
			want: 125 * 125,
		},
		{
			name: "multiply #4",
			args: args{a: 4.20, b: 100.00, function: multiply},
			want: 4.20 * 100.00,
		},
		{
			name: "multiply #5",
			args: args{a: 4, b: -3, function: multiply},
			want: 4 * -3,
		},
		{
			name: "multiply #6",
			args: args{a: -7, b: 6, function: multiply},
			want: -7 * 6,
		},
		{
			name: "multiply #7",
			args: args{a: -2, b: -2, function: multiply},
			want: -2 * -2,
		},
		{
			name: "multiply #8",
			args: args{a: -1233.45, b: 154.91, function: multiply},
			want: -1233.45 * 154.91,
		},
		// divide
		{
			name: "divide #1 ",
			args: args{a: 12, b: 12, function: divide},
			want: 12 / 12,
		},
		{
			name: "divide #2",
			args: args{a: 0, b: 1, function: divide},
			want: 0 / 1,
		},
		{
			name: "divide #3",
			args: args{a: 125, b: 125, function: divide},
			want: 125 / 125,
		},
		{
			name: "divide #4",
			args: args{a: 4.20, b: 100.00, function: divide},
			want: 4.20 / 100.00,
		},
		{
			name: "divide #5",
			args: args{a: 4, b: -3, function: divide},
			want: float64(4) / -3,
		},
		{
			name: "divide #6",
			args: args{a: -7, b: 6, function: divide},
			want: float64(-7) / 6,
		},
		{
			name: "divide #7",
			args: args{a: -2, b: -2, function: divide},
			want: -2 / -2,
		},
		{
			name: "divide #8",
			args: args{a: -1233.45, b: 1544.91, function: divide},
			want: -1233.45 / 1544.91,
		},
		//sum
		{
			name: "sum #1 ",
			args: args{a: 5, b: 5, function: sum},
			want: 10,
		},
		{
			name: "sum #2",
			args: args{a: 0, b: 0, function: sum},
			want: 0,
		},
		{
			name: "sum #3",
			args: args{a: 777, b: 777, function: sum},
			want: 1554,
		},
		{
			name: "sum #4",
			args: args{a: 5.42, b: 60.82, function: sum},
			want: 66.24,
		},
		{
			name: "sum #5",
			args: args{a: -9, b: 7, function: sum},
			want: -2,
		},
		{
			name: "sum #6",
			args: args{a: 9, b: -7, function: sum},
			want: 2,
		},
		{
			name: "sum #7",
			args: args{a: -9, b: -7, function: sum},
			want: -16,
		},
		{
			name: "sum #8",
			args: args{a: -41341.43, b: 1123.32, function: sum},
			want: -41341.43 + 1123.32,
		},
		// average
		{
			name: "average #1 ",
			args: args{a: 9, b: 8, function: average},
			want: (float64(9) + 8) / 2,
		},
		{
			name: "average #2",
			args: args{a: 2, b: 3, function: average},
			want: (float64(2) + 3) / 2,
		},
		{
			name: "average #3",
			args: args{a: 567, b: 544, function: average},
			want: (float64(567) + 544) / 2,
		},
		{
			name: "average #4",
			args: args{a: 5.42, b: 60.82, function: average},
			want: (5.42 + 60.82) / 2,
		},
		{
			name: "average #5",
			args: args{a: -9, b: 7, function: average},
			want: (-9 + 7) / 2,
		},
		{
			name: "average #6",
			args: args{a: 9, b: -7, function: average},
			want: (9 + -7) / 2,
		},
		{
			name: "average #7",
			args: args{a: -9, b: -7, function: average},
			want: (-9 + -7) / 2,
		},
		{
			name: "average #8",
			args: args{a: -5412.43, b: 63345.32, function: average},
			want: (-5412.43 + 63345.32) / 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calc.SetA(tt.args.a).SetB(tt.args.b).Do(tt.args.function).Result(); got != tt.want {
				t.Errorf("Result() = %v, want %v", got, tt.want)
			}
		})
	}
}
