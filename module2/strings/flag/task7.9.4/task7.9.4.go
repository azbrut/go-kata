package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app"`
	Production bool   `json:"is_prod"`
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	conf := flag.String("conf", "", "a string")
	flag.Parse()
	if *conf == "" {
		panic(errors.New("arg not found"))
	}

	file, err := os.ReadFile(*conf)
	check(err)

	var config Config
	err = json.Unmarshal(file, &config)
	check(err)

	fmt.Printf("%#v\n", config)
}
