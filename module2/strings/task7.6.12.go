package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	buf := bytes.NewBuffer(nil)
	// запишите данные в буфер
	for _, str := range data {
		buf.WriteString(str)
		buf.WriteString("\n")
	}
	// создайте файл
	f, err := os.Create("datafrombuffer.txt")
	check(err)

	// запишите данные в файл
	defer f.Close()
	//_, err = f.Write(buf.Bytes())
	_, err = io.Copy(f, buf)
	check(err)

	// прочтите данные в новый буфер
	Newbuf := bytes.NewBuffer(nil)
	f, err = os.Open("datafrombuffer.txt")
	check(err)
	defer f.Close()
	_, err = io.Copy(Newbuf, f)
	check(err)
	fmt.Println(Newbuf.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
