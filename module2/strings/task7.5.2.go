package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter your name: ")
	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	f, err := os.Create("/tmp/dat")
	check(err)
	defer f.Close()
	_, err = f.WriteString(name)
	check(err)
	// для проверки выполнить cat /tmp/dat

	data, err := ioutil.ReadFile("example.txt")
	check(err)
	transliter := strings.NewReplacer(
		"а", "a", "А", "A", "Б", "B", "б", "b", "В", "V", "в", "v", "Г", "G", "г", "g",
		"Д", "D", "д", "d", "З", "Z", "з", "z", "И", "I", "и", "i", "К", "K", "к", "k",
		"Л", "L", "л", "l", "М", "M", "м", "m", "Н", "N", "н", "n", "О", "O", "о", "o",
		"П", "P", "п", "p", "Р", "R", "р", "r", "С", "S", "с", "s", "Т", "T", "т", "t",
		"У", "U", "у", "u", "Ф", "F", "ф", "f", "Е", "E", "е", "e", "Ё", "Yo", "ё", "yo",
		"Ж", "Zh", "ж", "zh", "Й", "J", "й", "j", "Х", "H", "х", "h", "Ч", "Ch", "ч", "ch",
		"Ш", "Sh", "ш", "sh", "Щ", "Shh", "щ", "shh", "Ъ", "``", "ъ", "``", "Ы", "Y`",
		"ы", "y`", "Ь", "`", "ь", "`", "Э", "E`", "э", "e`", "Ю", "Yu", "ю", "yu", "Я", "Ya", "я", "ya")
	latinData := transliter.Replace(string(data))
	f, err = os.Create("example.processed.txt")
	check(err)
	_, err = f.WriteString(latinData)
	check(err)
}
