package main

import (
	"fmt"
	"time"
)

func main() {
	/*message := make(chan string, 2)
	message <- "hello"
	message <- "world"
	fmt.Println(<-message)
	message <- "!"
	fmt.Println(<-message)
	fmt.Println(<-message)*/
	/*message := make(chan string)
	//go func() {
	//	time.Sleep(2 * time.Second)
	//	message <- "hello"
	}()
	//message <- "hello"
	//msg := <-message
	//fmt.Println(<-message)
	go func() {
		for i := 1; i <= 10; i++ {
			message <- fmt.Sprintf("%d", i)
			time.Sleep(time.Millisecond * 500)
		}
		close(message)
	}()
	//for {
	//	msg, open := <-message
	//	if !open {
	//		break
	//	}
	//	fmt.Println(msg)
	//}
	for msg := range message {
		fmt.Println(msg)
	}*/

	/*urls := []string{
		"https://google.com/",
		"https://youtube.com/",
		"https://github.com/",
		"https://medium.com/",
	}
	var wg sync.WaitGroup
	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			doHTTP(url)
			wg.Done()
		}(url)
	}
	wg.Wait()*/
	message1 := make(chan string)
	message2 := make(chan string)
	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			message1 <- "Прошло пол секунды"
		}
	}()
	go func() {
		for {
			time.Sleep(time.Millisecond * 2000)
			message2 <- "Прошло 2 секунды"
		}
	}()
	for {
		select {
		case msg := <-message1:
			fmt.Println(msg)
		case msg := <-message2:
			fmt.Println(msg)
		}
		//fmt.Println(<-message1)
		//fmt.Println(<-message2)
	}
}

/*func doHTTP(url string) {
	t := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Failed to get <%s>: %s\n", url, err.Error())
	}
	defer resp.Body.Close()
	fmt.Printf("<%s> - Status code [%d] - Latency %d ms\n",
		url, resp.StatusCode, time.Since(t).Milliseconds())
}*/
