package main

import (
	"testing"
)

type args struct {
	name string
}

func TestGreet(t *testing.T) {
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		var greet = Greet(tt.args.name, tt.name)
		t.Run(tt.name, func(t *testing.T) {
			if got := greet; got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}
