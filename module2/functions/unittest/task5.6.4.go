package main

import (
	"fmt"
)

func Greet(name, language string) string {
	if language == "english name" {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
}
